plugins {
    kotlin("jvm").version("1.4.30")
    application
}

repositories {
    jcenter()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
}

application {
    mainClassName = "com.apomelov.units.AppKt"
}
