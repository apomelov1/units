package com.gitlab.apomelov1.units

// =================
// Base units
val Number.s get() = PhysValue(toDouble(), com.gitlab.apomelov1.units.s)
val Number.ms get() = s / 1000
val Number.us get() = ms / 1000
val Number.ns get() = us / 1000

val Number.m get() = PhysValue(toDouble(), com.gitlab.apomelov1.units.m)
val Number.mm get() = m / 1000
val Number.um get() = mm / 1000
val Number.nm get() = um / 1000
val Number.km get() = m * 1000

val Number.kg get() = PhysValue(toDouble(), com.gitlab.apomelov1.units.kg)
val Number.g get() = kg / 1000
val Number.mg get() = g / 1000
val Number.ug get() = mg / 1000
val Number.ng get() = ug / 1000

val Number.A get() = PhysValue(toDouble(), com.gitlab.apomelov1.units.A)
val Number.mA get() = A / 1000
val Number.uA get() = mA / 1000
val Number.nA get() = uA / 1000
val Number.kA get() = A * 1000

val Number.K get() = PhysValue(toDouble(), com.gitlab.apomelov1.units.K)
val Number.mol get() = PhysValue(toDouble(), com.gitlab.apomelov1.units.mol)
val Number.cd get() = PhysValue(toDouble(), com.gitlab.apomelov1.units.cd)


// =================
// Derived units
val Number.rad get() = PhysValue(toDouble(), com.gitlab.apomelov1.units.rad)
val Number.sr get() = PhysValue(toDouble(), com.gitlab.apomelov1.units.sr)

val Number.Hz get() = PhysValue(toDouble(), com.gitlab.apomelov1.units.Hz)
val Number.kHz get() = Hz * 1000
val Number.MHz get() = kHz * 1000
val Number.GHz get() = MHz * 1000

val Number.N get() = PhysValue(toDouble(), com.gitlab.apomelov1.units.N)
val Number.kN get() = N * 1000

val Number.Pa get() = PhysValue(toDouble(), com.gitlab.apomelov1.units.Pa)
val Number.kPa get() = Pa * 1000
val Number.MPa get() = kPa * 1000

val Number.J get() = PhysValue(toDouble(), com.gitlab.apomelov1.units.J)
val Number.kJ get() = J * 1000
val Number.MJ get() = kJ * 1000

val Number.W get() = PhysValue(toDouble(), com.gitlab.apomelov1.units.W)
val Number.mW get() = W / 1000
val Number.kW get() = W * 1000

val Number.C get() = PhysValue(toDouble(), com.gitlab.apomelov1.units.C)

val Number.V get() = PhysValue(toDouble(), com.gitlab.apomelov1.units.V)
val Number.mV get() = V / 1000
val Number.kV get() = V * 1000

val Number.F get() = PhysValue(toDouble(), com.gitlab.apomelov1.units.F)
val Number.mF get() = F / 1000
val Number.uF get() = mF / 1000
val Number.nF get() = uF / 1000

val Number.ohm get() = PhysValue(toDouble(), com.gitlab.apomelov1.units.ohm)
val Number.kohm get() = ohm * 1000
val Number.Mohm get() = kohm * 1000

val Number.siemens get() = PhysValue(toDouble(), com.gitlab.apomelov1.units.siemens)
val Number.Wb get() = PhysValue(toDouble(), com.gitlab.apomelov1.units.Wb)
val Number.T get() = PhysValue(toDouble(), com.gitlab.apomelov1.units.T)
val Number.H get() = PhysValue(toDouble(), com.gitlab.apomelov1.units.H)
val Number.lm get() = PhysValue(toDouble(), com.gitlab.apomelov1.units.lm)
val Number.lx get() = PhysValue(toDouble(), com.gitlab.apomelov1.units.lx)
val Number.Bq get() = PhysValue(toDouble(), com.gitlab.apomelov1.units.Bq)
val Number.Gy get() = PhysValue(toDouble(), com.gitlab.apomelov1.units.Gy)
val Number.Sv get() = PhysValue(toDouble(), com.gitlab.apomelov1.units.Sv)
val Number.kat get() = PhysValue(toDouble(), com.gitlab.apomelov1.units.kat)
