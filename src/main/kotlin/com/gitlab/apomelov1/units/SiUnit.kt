package com.gitlab.apomelov1.units

import kotlin.math.abs


private const val DOT = "∙"
private val POWERS = arrayOf("⁰", "", "²", "³", "⁴", "⁵", "⁶", "⁷", "⁸", "⁹")

private fun Int.asPowerString(): String {
    val abs = abs(this)
    return if (abs < 10) POWERS[abs] else "^$abs"
}

open class SiUnit(private val units: Map<SiBaseUnit, Int>, private val symbol: String? = null) {

    constructor(baseUnit: SiBaseUnit) : this(mapOf(baseUnit to 1), baseUnit.symbol)
    constructor(from: SiUnit, symbol: String) : this(from.units, symbol)

    fun register() = this.also {
        if (symbol != null) {
            dict.putIfAbsent(units, this)
        }
    }

    private fun resolve() = dict[units] ?: this

    operator fun times(other: SiUnit): SiUnit =
            SiUnit((units.asSequence() + other.units.asSequence())
                    .groupBy({ it.key }, { it.value })
                    .mapValues { it.value.sum() }
                    .filter { it.value != 0 }
            ).resolve()

    operator fun div(other: SiUnit) = this * SiUnit(other.units.mapValues { -it.value })

    override fun toString(): String {
        return if (symbol != null) {
            symbol
        } else {
            val numerator = units.entries
                    .filter { it.value > 0 }
                    .joinToString(DOT) { (unit, power) -> "${unit.symbol}${power.asPowerString()}" }
                    .takeIf { it.isNotBlank() }
                    ?: "1"
            val denominator = units.entries
                    .filter { it.value < 0 }
                    .joinToString(DOT) { (unit, power) -> "${unit.symbol}${power.asPowerString()}" }
                    .takeIf { it.isNotBlank() }
                    ?.let { "/$it" }
                    ?: ""
            "$numerator$denominator"
        }
    }

    companion object {
        val dict = hashMapOf<Map<SiBaseUnit, Int>, SiUnit>()
    }

}
