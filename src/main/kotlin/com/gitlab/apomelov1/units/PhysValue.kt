package com.gitlab.apomelov1.units


data class PhysValue(val value: Double, val unit: SiUnit) {
    operator fun plus(other: PhysValue): PhysValue {
        check(unit == other.unit) { "Incompatible units: $unit and ${other.unit}" }
        return PhysValue(value + other.value, unit)
    }

    operator fun unaryMinus() = PhysValue(-value, unit)
    operator fun minus(other: PhysValue) = this + (-other)
    operator fun times(other: PhysValue) = PhysValue(value * other.value, unit * other.unit)
    operator fun div(other: PhysValue) = PhysValue(value / other.value, unit / other.unit)

    operator fun plus(other: Number) = this + PhysValue(1.0, scalar)
    operator fun minus(other: Number) = this - PhysValue(1.0, scalar)
    operator fun times(other: Number) = PhysValue(value * other.toDouble(), unit)
    operator fun div(other: Number) = PhysValue(value / other.toDouble(), unit)

    override fun toString() = "$value $unit"
}

operator fun Number.plus(other: PhysValue) = PhysValue(toDouble(), scalar) + other
operator fun Number.minus(other: PhysValue) = PhysValue(toDouble(), scalar) - other
operator fun Number.times(other: PhysValue) = PhysValue(toDouble() * other.value, other.unit)
operator fun Number.div(other: PhysValue) = PhysValue(toDouble() / other.value, scalar / other.unit)
