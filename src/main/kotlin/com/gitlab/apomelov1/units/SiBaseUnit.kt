package com.gitlab.apomelov1.units


enum class SiBaseUnit(val symbol: String) {
    SECOND("s"),
    METRE("m"),
    KILOGRAM("kg"),
    AMPERE("A"),
    KELVIN("K"),
    MOLE("mol"),
    CANDELA("cd")
}
