package com.gitlab.apomelov1.units


operator fun <K> Map<K, Int>.times(other: Map<K, Int>): Map<K, Int> =
        (asSequence() + other.asSequence())
                .groupBy({ it.key }, { it.value })
                .mapValues { it.value.sum() }
                .filter { it.value != 0 }

operator fun <K> Map<K, Int>.div(other: Map<K, Int>): Map<K, Int> =
        this * (other.mapValues { -it.value })
